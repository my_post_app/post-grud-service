package grpc

import (
	"post_grud_service/config"
	"post_grud_service/grpc/service"
	"post_grud_service/pkg/logger"
	"post_grud_service/storage"

	"post_grud_service/genproto/post_grud_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	post_grud_service.RegisterPostGrudServiceServer(grpcServer, service.NewPostGrudService(cfg, log, strg))
	reflection.Register(grpcServer)
	return
}
