package service

import (
	"context"
	"fmt"
	"post_grud_service/config"
	"post_grud_service/genproto/post_grud_service"
	pbu "post_grud_service/genproto/post_grud_service"
	"post_grud_service/pkg/logger"
	"post_grud_service/storage"

	"github.com/golang/protobuf/ptypes/empty"
)

type postService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	post_grud_service.UnimplementedPostGrudServiceServer
}

func NewPostGrudService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) *postService {
	return &postService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s *postService) GetPosts(ctx context.Context, req *empty.Empty) (res *pbu.GetPostsResponse, err error) {
	s.log.Info("GetPosts calling ...")
	res, err = s.strg.PostgresStorage().PostsStorage().GetPosts(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("Error occured while getting all posts: %s", err.Error()))
		return nil, err
	}
	return res, nil
}

func (s *postService) GetPostByID(ctx context.Context, req *pbu.IdReq) (res *pbu.Post, err error) {
	s.log.Info("GetPosts calling ...")
	res, err = s.strg.PostgresStorage().PostsStorage().GetPostByID(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("Error occured while getting post by id: %s", err.Error()))
		return nil, err
	}
	return res, nil
}

func (s *postService) UpdatePost(ctx context.Context, req *pbu.Post) (res *pbu.Response, err error) {
	s.log.Info("GetPosts calling ...")
	res, err = s.strg.PostgresStorage().PostsStorage().UpdatePost(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("Error occured while updating posts: %s", err.Error()))
		return nil, err
	}
	return res, nil
}

func (s *postService) DeletePost(ctx context.Context, req *pbu.IdReq) (res *pbu.Response, err error) {
	s.log.Info("GetPosts calling ...")
	res, err = s.strg.PostgresStorage().PostsStorage().DeletePost(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("Error occured while deleting posts: %s", err.Error()))
		return nil, err
	}
	return res, nil
}
