package helper

import (
	"math/rand"
	"time"
)

func RandomID() string {
	const (
		randomStringLength = 3
		randomIntLength    = 5
	)
	var randomBytes []byte
	rand.Seed(time.Now().UnixMicro())
	for i := 0; i < randomStringLength; i++ {
		randomBytes = append(randomBytes, byte(rand.Intn(26)+65))
	}
	for i := 0; i < randomIntLength; i++ {
		randomBytes = append(randomBytes, byte(rand.Intn(10)+48))
	}
	return string(randomBytes)
}
