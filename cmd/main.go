package main

import (
	"context"
	"net"
	"post_grud_service/config"
	"post_grud_service/grpc"
	"post_grud_service/pkg/logger"
	"post_grud_service/storage"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()
	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	database, err := storage.NewStorage(context.Background(), cfg)
	if err != nil {
		log.Panic("storage.NewStorage", logger.Error(err))
	}
	defer database.Close()

	grpcServer := grpc.SetUpServer(cfg, log, database)

	lis, err := net.Listen("tcp", cfg.PostsGrudGRPCPort)
	if err != nil {
		log.Panic("net.Listen", logger.Error(err))
	}

	done := make(chan bool)
	go func() {
		defer func() { done <- true }()
		log.Info("GRPC: Server being started...", logger.String("port", cfg.PostsGrudGRPCPort))
		if err := grpcServer.Serve(lis); err != nil {
			log.Panic("grpcServer.Serve", logger.Error(err))
		}
	}()
	<-done
}
