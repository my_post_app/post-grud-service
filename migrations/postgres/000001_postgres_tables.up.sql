DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'post_service') THEN

CREATE ROLE post_service SUPERUSER ;
END IF;
END
$do$;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";