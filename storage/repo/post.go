package repo

import (
	"context"
	pbu "post_grud_service/genproto/post_grud_service"

	"github.com/golang/protobuf/ptypes/empty"
)

type PostsI interface {
	// CreatePosts(ctx context.Context, req *entity.Post) (res *pbu.CreatePostResponse, err error)
	GetPosts(ctx context.Context, req *empty.Empty) (res *pbu.GetPostsResponse, err error)
	GetPostByID(ctx context.Context, req *pbu.IdReq) (res *pbu.Post, err error)
	UpdatePost(ctx context.Context, req *pbu.Post) (res *pbu.Response, err error)
	DeletePost(ctx context.Context, req *pbu.IdReq) (res *pbu.Response, err error)
}
