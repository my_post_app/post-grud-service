package storage

import (
	"context"
	"errors"
	"post_grud_service/config"
	"post_grud_service/storage/postgres"
	"post_grud_service/storage/repo"
)

var ErrorTheSameId = errors.New("cannot use the same uuid for 'id' and 'parent_id' fields")

type StorageI interface {
	PostgresStorage() repo.PostgresStorageI
	Close()
}
type Storage struct {
	postgresStorage repo.PostgresStorageI
}

func NewStorage(ctx context.Context, cfg config.Config) (StorageI, error) {
	postgresDB, err := postgres.NewPostgres(ctx, cfg)
	if err != nil {
		return nil, err
	}
	return &Storage{
		postgresStorage: postgresDB,
	}, nil
}

func (s *Storage) PostgresStorage() repo.PostgresStorageI {
	return s.postgresStorage
}

func (s *Storage) Close() {
	s.postgresStorage.CloseDB()
}
