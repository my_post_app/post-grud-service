package postgres

import (
	"context"
	pbu "post_grud_service/genproto/post_grud_service"
	"post_grud_service/storage/repo"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type postRepo struct {
	db *pgxpool.Pool
}

func NewPostGrud(db *pgxpool.Pool) repo.PostsI {
	return &postRepo{db}
}

func (i *postRepo) GetPosts(ctx context.Context, req *empty.Empty) (res *pbu.GetPostsResponse, err error) {

	stmt := `SELECT
			id,
			user_id,
			title,
			body
		FROM posts`
	rows, err := i.db.Query(ctx, stmt)

	if err != nil {
		return
	}
	defer rows.Close()
	res = &pbu.GetPostsResponse{}

	for rows.Next() {
		var post pbu.Post
		err = rows.Scan(&post.Id, &post.UserId, &post.Title, &post.Body)
		if err != nil {
			return
		}
		res.Posts = append(res.Posts, &post)
	}

	queryCount := `
	SELECT COUNT(*) 
	FROM posts`
	err = i.db.QueryRow(ctx, queryCount).Scan(&res.Count)
	if err == pgx.ErrNoRows {
		err = nil
	}
	return
}

func (i *postRepo) UpdatePost(ctx context.Context, req *pbu.Post) (res *pbu.Response, err error) {
	res = &pbu.Response{}
	stmt := `
	UPDATE posts SET user_id = $1, title = $2, body = $3
	WHERE id = $4`

	err = i.db.QueryRow(ctx, stmt, req.UserId, req.Title, req.Body, req.Id).Scan()
	if err == pgx.ErrNoRows {
		err = nil
		res.Message = "element not found"
	}
	res.Message = "successfully updated"
	return
}

func (i *postRepo) DeletePost(ctx context.Context, req *pbu.IdReq) (res *pbu.Response, err error) {
	res = &pbu.Response{}

	stmt := `
	DELETE FROM posts
	WHERE id = $1 `
	err = i.db.QueryRow(ctx, stmt, req.Id).Scan()
	if err == pgx.ErrNoRows {
		err = nil
		res.Message = "element not found"

	}
	res.Message = "successfully deleted"

	return
}

func (i *postRepo) GetPostByID(ctx context.Context, req *pbu.IdReq) (res *pbu.Post, err error) {
	res = &pbu.Post{}

	stmt := `SELECT
			id,
			user_id,
			title,
			body
		FROM posts where id = $1`
	err = i.db.QueryRow(ctx, stmt, req.GetId()).Scan(&res.Id, &res.UserId, &res.Title, &res.Body)
	if err == pgx.ErrNoRows {
		return nil, err
	}
	return
}
